package edu.idatt.mappeto.marcusjohannessen;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * @author marcusjohannessen
 * Factory design, not really necessary here
 * because the frontend is coded in fxml.
 */

public abstract class Factory {

    public static Node createFactory(String nodename){
        if(nodename.isBlank()){
            return null;
        }else if(nodename.equalsIgnoreCase("tableView")){
            return new TableView<>();
        }else if(nodename.equalsIgnoreCase("button")){
            return new Button();
        }else if(nodename.equalsIgnoreCase("vbox")){
            return new VBox();
        }else if(nodename.equalsIgnoreCase("toolbar")){
            return new ToolBar();
        }else if(nodename.equalsIgnoreCase("menubar")){
            return new MenuBar();
        }else if(nodename.equalsIgnoreCase("textfield")){
            return new TextField();
        }else if(nodename.equalsIgnoreCase("gridpane")){
            return new GridPane();
        }
        return null;
    }
}