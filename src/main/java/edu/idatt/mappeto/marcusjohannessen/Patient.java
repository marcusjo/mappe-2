package edu.idatt.mappeto.marcusjohannessen;


public class Patient {

    private String firstName;
    private String surName;
    private String socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;


    public Patient(String firstName, String surName,String generalPractitioner, String socialSecurityNumber){
        this.firstName = firstName;
        this.surName = surName;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public Patient(String firstName, String surName, String socialSecurityNumber,
                   String diagnosis, String generalPractitioner) {
        this.firstName = firstName;
        this.surName = surName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     *
     * @param obj
     * @return true or false
     * Checks if Patient equals another
     * one by social security number.
     * one by social security number.
     */

    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(!(obj instanceof Patient)){
            return false;
        }
        Patient otherPatient = (Patient) obj;
        return socialSecurityNumber.equals(
                otherPatient.getSocialSecurityNumber());
    }

    /**
     *
     * @return String of patient information
     * Semicolon because of reading
     * and writing to CSV
     */
    @Override
    public String toString() {
        return "Patient\n" +
                ";" + firstName +
                ";" + surName +
                ";" + socialSecurityNumber +
                ";" + diagnosis +
                ";" + generalPractitioner;
    }
}
