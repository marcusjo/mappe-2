package edu.idatt.mappeto.marcusjohannessen.data;


import edu.idatt.mappeto.marcusjohannessen.Patient;
import edu.idatt.mappeto.marcusjohannessen.PatientRegister;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author marcusjohannessen
 * Class that writes and read from csv file
 */

public class Filehandler<T extends Patient> {

    private FileWriter fileWriter;
    PatientRegister patientRegister;
    private static final String COMMA_DEMLIMITTER = ";";
    private static final String NEW_LINE = "\n";

    /**
     * constructor
     */
    public Filehandler() {
        patientRegister = new PatientRegister();
    }

    /**
     * Stores data of patient to a file by CSV writer
     */
    public void writeToCSV(ObservableList<T> listPatients, File filePath) throws IOException {
        try {
            fileWriter = new FileWriter(filePath);

            //fileWriter.append(NEW_LINE);
            //TODO: Fikse den der nullpointerExeption som kommer her
            for (Patient p: listPatients){
                fileWriter.append(String.valueOf(p.getFirstName()));
                fileWriter.append(COMMA_DEMLIMITTER);
                fileWriter.append(String.valueOf(p.getSurName()));
                fileWriter.append(COMMA_DEMLIMITTER);
                fileWriter.append(String.valueOf(p.getGeneralPractitioner()));
                fileWriter.append(COMMA_DEMLIMITTER);
                fileWriter.append(String.valueOf(p.getSocialSecurityNumber()));
                fileWriter.append(NEW_LINE);
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                if(fileWriter != null){
                    fileWriter.close(); //closes the file writer output stream
                }
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }

    /**
     * Reads patient information from a csv file.
     */
    public List<Patient> readFromCSV(File filePath) throws IOException{
        BufferedReader buffReader = null;
        List<Patient> patientList = new ArrayList<Patient>();
        try{
            String line = "";

            buffReader = new BufferedReader(new FileReader(filePath));
            //buffReader.readLine();
            while ((line = buffReader.readLine()) != null){
                String[] tokens = line.split(COMMA_DEMLIMITTER);
                if (tokens.length > 0){
                    try {
                        Patient patient = new Patient(tokens[0], tokens[1], tokens[2], tokens[3]);
                        patientList.add(patient);
                    }catch (IndexOutOfBoundsException iob){
                        iob.printStackTrace();
                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally { //Enter this independent of what happens
            try{
                buffReader.close();
            }catch (IOException | NullPointerException exception){
                System.out.println("You know you fucked up right? byebye wish you luck with life");
                exception.printStackTrace();
            }
        }
        return patientList;
    }
}