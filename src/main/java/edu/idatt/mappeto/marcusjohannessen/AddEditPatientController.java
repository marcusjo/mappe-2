package edu.idatt.mappeto.marcusjohannessen;

import javafx.fxml.FXML;

import javafx.scene.control.TextField;


public class AddEditPatientController {

    @FXML
    private TextField firstName;
    @FXML
    private TextField surName;
    @FXML
    private TextField generalPractitioner;
    @FXML
    private TextField socialSecurityNumber;


    public Patient processPatient(){
        String fn = firstName.getText().trim();
        String sn = surName.getText().trim();
        String gp = generalPractitioner.getText().trim();
        String ssn = socialSecurityNumber.getText().trim();

        return new Patient(fn, sn, gp, ssn);
    }
}