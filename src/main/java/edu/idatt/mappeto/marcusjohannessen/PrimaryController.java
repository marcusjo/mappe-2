package edu.idatt.mappeto.marcusjohannessen;

import edu.idatt.mappeto.marcusjohannessen.data.Filehandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

public class PrimaryController {

    @FXML
    private VBox mainVbox;
    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, String> firstNameCol;
    @FXML
    private TableColumn<Patient, String> surNameCol;
    @FXML
    private TableColumn<Patient, String> genPracCol;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumberCol;
    @FXML
    private Label amountPatients;

    PatientRegister patientRegister = new PatientRegister();
    Filehandler<Patient> fh = new Filehandler<>();

    /**
     * Initialize the main window
     */
    public void initialize() {
        setCellProperty();
        tableView.setItems(patientRegister.getPatients());
        amountPatients.setText("Amount of Patients: " + String.valueOf(tableView.getItems().size()));

    }

    private void setCellProperty() {
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        surNameCol.setCellValueFactory(new PropertyValueFactory<>("surName"));
        genPracCol.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        socialSecurityNumberCol.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
    }
    private void setAmountPatients(){
        amountPatients.setText("Amount of Patients: " + String.valueOf(tableView.getItems().size()));
    }

    @FXML
    public void handleAboutButton() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("hei hallo");
        alert.setContentText("Patients Register\nv0.1-SNAPSHOT");
        alert.show();
    }


    /**
     * Displays an error message
     *
     * @param message
     */
    private void errorMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Error");
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Method for adding patient in the application
     */
    @FXML
    public void addNewPatient() {
        Patient p = addEditSelectedPatient("Add", "Use this to add a new patient");
        try {
            if (patientRegister.getPatients().contains(p)) {
                throw new IllegalArgumentException("Patient already exists");
            } else {
                assert p != null;
                //This gives NullpointerException if socailSecurityNumber does not exist, fuck!!!
                if (p.getFirstName().isEmpty()) {
                    errorMessage("No First Name Detected");
                } else if (p.getSurName().isEmpty()) {
                    errorMessage("No Surname Detected");
                } else if (p.getGeneralPractitioner().isEmpty()) {
                    errorMessage("No general practitioner detected");
                } else if ((p.getSocialSecurityNumber().length() != 11) &&
                        !p.getSocialSecurityNumber().matches("[0-9]]")) {
                    errorMessage("Social Security Number have to be 11 numbers");
                } else {
                    patientRegister.addPatient(p); //add to ObservableList in FileHandler for storing it
                    tableView.setItems(patientRegister.getPatients()); //Sets from observable list in Fi
                    setAmountPatients();
                }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Edits and Patient in the register
     */
    @FXML
    public void editNewPatient() {
        try {
            Patient pat = addEditSelectedPatient("Edit", "Use this to edit selected patient");
            assert pat != null;

            if (pat.getFirstName().isBlank()) {
                errorMessage("No First Name Detected");
            } else if (pat.getSurName().isBlank()) {
                errorMessage("No Surname Detected");
            } else if (pat.getGeneralPractitioner().isBlank()) {
                errorMessage("No general practitioner detected ");
            } else if ((pat.getSocialSecurityNumber().length() != 11) &&
                    !pat.getSocialSecurityNumber().matches("[0-9]]")) {
                errorMessage("Social Security Number have to be 11 numbers");
            } else {
                patientRegister.editPatient(tableView.getSelectionModel().getSelectedItem(),
                        pat.getFirstName(), pat.getSurName(), pat.getGeneralPractitioner(), pat.getSocialSecurityNumber());
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        tableView.refresh(); //Fyfaen for en nice methode!!!!
        setAmountPatients();
    }

    /**
     * Method for delete in GUI
     */
    @FXML
    public void handleRemoveButton() {
        Patient p = tableView.getSelectionModel().getSelectedItem();
        if (p != null) {
            removePatient(p);
        }
    }

    /**
     * Method for removing patient
     */
    public void removePatient(Patient patient) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete Confirmation");
        alert.setContentText("Delete " + patient.getFirstName());
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            patientRegister.deletePatient(patient);   //Delte from list in filehandler
            tableView.refresh();
            setAmountPatients();
        }
    }

    /**
     * @return patient
     * Calls DialogPane window for adding or editing a patient
     */
    private Patient addEditSelectedPatient(String action, String message) {
        Dialog<ButtonType> editDialog = new Dialog<>();
        editDialog.initOwner(mainVbox.getScene().getWindow());
        editDialog.setTitle(action + " Patient");
        editDialog.setHeaderText(message);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("addAndEditPatient.fxml"));
        try {
            editDialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Because Dialog is Button type, button OK and CANCEL is put
        //Down right by default
        editDialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        editDialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> result = editDialog.showAndWait();

        if (result.isPresent() && result.get() == ButtonType.OK) {
            AddEditPatientController aec = fxmlLoader.getController();
            return aec.processPatient();
        } else {
            return null;
        }
    }


    /*
     *Reads from CSV in the root folder
     */
    @FXML
    public void importFromCSV() {
        File filePath = new File("src/main/resources/edu/idatt/mappeto/marcusjohannessen/storage/patients_storage.csv");
        try {
            //Convert to Observable for putting in tableview
            ObservableList<Patient> obList = FXCollections.observableArrayList(fh.readFromCSV(filePath));
            patientRegister.getPatients().clear();
            patientRegister.addAllFromList(obList);
            tableView.setItems(patientRegister.getPatients());
            tableView.refresh();
            setAmountPatients();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes to CSV in root folder
     */
    @FXML
    public void exportToCSV() {
        File filePath = new File("src/main/resources/edu/idatt/mappeto/marcusjohannessen/storage/patients_storage.csv");
        try {
            fh.writeToCSV(patientRegister.getPatients(), filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isCSV(File file){
        String[] listFileName;
        String fileName = (file != null) ? file.getName() : "";
        try{
            listFileName = fileName.split("[.]");
            String fileType = (listFileName.length > 0) ? listFileName[1] : "";
            if (fileType.isBlank() || !fileType.equals("csv")) {
                return false;
            }
        }catch (ArrayIndexOutOfBoundsException | NullPointerException e){
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Opens Finder (On Mac) and allows the user to only choose
     * data from a .csv file in a directory.
     */
    @FXML
    public void readFromFile() throws FileNotFoundException {
        ObservableList<Patient> observableList;
        FileChooser fc = new FileChooser();
        fc.setTitle("Choose a file to read from??");
        boolean run = true;


        File file = fc.showOpenDialog(mainVbox.getScene().getWindow());
        while (run) {

            if (isCSV(file)) {
                try {
                    observableList = FXCollections.observableArrayList(fh.readFromCSV(file));
                    patientRegister.getPatients().clear(); //Clear patient register for patients from other file
                    patientRegister.addAllFromList(observableList); //User should save in a file or risk losing all data in tableview
                    tableView.setItems(patientRegister.getPatients());
                    tableView.refresh();
                    setAmountPatients();
                    run = false;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                    //If choosen filetype is not .csv
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("WRONG!!");
                    alert.setContentText("The chosen file type is not valid. " +
                            "\nPlease choose another file by clicking on " +
                            "\nSelect File or cancel the operation");
                    alert.showAndWait();
                    file = fc.showOpenDialog(mainVbox.getScene().getWindow());

                if (file == null) {
                    run = false;
                }
            }
        }
    }
    /**
     * Chooser where user want to save new CSV file of Patients
     */
    @FXML
    public void writeToFile() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Choose where you want to save");
        fc.getExtensionFilters().addAll(
                //Makes sure that the file is .csv
                new FileChooser.ExtensionFilter("CSV", "*.csv")
        );

        File file = fc.showSaveDialog(mainVbox.getScene().getWindow());

        try {
            //Writes data from patientRegister to file
            fh.writeToCSV(patientRegister.getPatients(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}