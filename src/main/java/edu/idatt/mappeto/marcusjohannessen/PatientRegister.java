package edu.idatt.mappeto.marcusjohannessen;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.Iterator;

/**
 * @author marcusjohannessen
 * Class that handles the register of patients
 *
 */

public class PatientRegister {
    private ObservableList<Patient> patients;


    /**
    * Constructor
     * initialize the patients list
     */
    public PatientRegister() {
        this.patients = FXCollections.observableArrayList();
    }

    public ObservableList<Patient> getPatients() {
        return patients;
    }

    public void addPatient(Patient patient){
        patients.add(patient);
    }

    public void deletePatient(Patient patient){
        patients.remove(patient);
    }

    /**
     *
     * @param list
     * adds patients from a list to
     * patient list in patient register.
     */
    public void addAllFromList(ObservableList<Patient> list){
        if(list.isEmpty()){
            return;
        }else{
            patients.addAll(list);
        }
    }

    /**
     *
     * @param patient
     * @param newFirstName
     * @param newSurName
     * @param generalPractitioner
     * @param newSocialSecNumb
     * Method for editing a patient
     * new values from first parameter patient
     * which is selected from tableview
     *
     */
    public void editPatient(Patient patient, String newFirstName
        , String newSurName,String generalPractitioner, String newSocialSecNumb) throws NullPointerException {
        //Iterator because we want to change the list
        //while looping over it (change the state of the list).
        Iterator<Patient> iter = patients.iterator();
        while (iter.hasNext()) {
            Patient p = iter.next();
            if (p.getFirstName().equalsIgnoreCase(patient.getFirstName())) {
                p.setFirstName(newFirstName);
                p.setSurName(newSurName);
                p.setGeneralPractitioner(generalPractitioner);
                p.setSocialSecurityNumber(newSocialSecNumb);
            }
        }
    }
}