module edu.idatt.mappeto.marcusjohannessen {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.idatt.mappeto.marcusjohannessen to javafx.fxml;
    exports edu.idatt.mappeto.marcusjohannessen;
}