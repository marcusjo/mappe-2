package edu.idatt.mappeto.marcusjohannessen;

import edu.idatt.mappeto.marcusjohannessen.data.Filehandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {


    static Patient patient1;
    static Patient patient2;
    static Patient patient3;
    static Patient patient4;
    static Filehandler<Patient> filehandler;
    static PatientRegister patientRegister;


    @BeforeAll
    static void setUp(){
        patient1 = new Patient("Patient1", "Pat1", "genPrac1", "12345678900");
        patient2 = new Patient("Patient2", "Pat2", "genPrac2", "12345678901");
        filehandler = new Filehandler<>();
        patientRegister = new PatientRegister();
        patientRegister.addPatient(patient1);
        patientRegister.addPatient(patient2);
        patientRegister.addPatient(patient4);

    }

    @Test
    void should_return_all_patients_in_list() {
        Assertions.assertEquals(patient1.getSocialSecurityNumber(), patientRegister.getPatients().get(0).getSocialSecurityNumber());
        //Assertions.assertEquals(patient2.getSocialSecurityNumber(), patientRegister.getPatients().get(1).getSocialSecurityNumber());
    }

    @Test
    void should_add_patient_to_list() {
        patient3 = new Patient("Patient3", "Pat3", "genPrac3", "12345678902");
        patientRegister.addPatient(patient3);
        Assertions.assertTrue(patientRegister.getPatients().contains(patient3));
    }

    @Test
    void should_delete_patient_from_list() {
        patient4 = new Patient("Patient4", "Pat4", "genPrac4", "12345678904");
        patientRegister.deletePatient(patient4);
        Assertions.assertFalse(patientRegister.getPatients().contains(patient4));
    }
}