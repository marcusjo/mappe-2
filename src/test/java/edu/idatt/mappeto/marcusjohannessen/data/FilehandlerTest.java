package edu.idatt.mappeto.marcusjohannessen.data;

import edu.idatt.mappeto.marcusjohannessen.Patient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FilehandlerTest {

    static File testFilePath;
    static Filehandler<Patient> testFilehandler;
    static ObservableList<Patient> testPatients;
    static Patient patient1;
    static Patient patient2;


    @BeforeAll
    static void setUp(){
        testFilePath = new File("src/test/resources/storageTest/patient_storage_test.csv");
        testFilehandler = new Filehandler<>();
        testPatients = FXCollections.observableArrayList();
        patient1 = new Patient("Patient1", "Pat1", "genPrac1", "12345678900");
        patient2 = new Patient("Patient2", "Pat2", "genPrac2", "12345678901");

        testPatients.add(patient1);
        testPatients.add(patient2);
    }


    @Test
    @DisplayName("Checks if write to file do not throw exception")
    void should_write_patients_to_CSV_file() {
        try{
            testFilehandler.writeToCSV(testPatients, testFilePath);
        }catch (IOException ioe){
            fail(ioe);
        }
    }

    @Test
    @DisplayName("Check if list written to csv contains same patients written to csv")
    void csv_should_contain_same_patients_written_to_it() {
        try{
            List<Patient> list = testFilehandler.readFromCSV(testFilePath);
            Assertions.assertEquals(list.get(0), patient1);
            Assertions.assertEquals(list.get(1), patient2);

        }catch (IOException ioe){
            fail(ioe);
        }
    }
}