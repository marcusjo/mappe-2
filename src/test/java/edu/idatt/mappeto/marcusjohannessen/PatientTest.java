package edu.idatt.mappeto.marcusjohannessen;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {

    static Patient patient;

    @BeforeAll
    static void setUp(){
        patient = new Patient("Patient","Yo","genprac","12345678900");
    }

    @Test
    void getFirstName() {
        Assertions.assertEquals(patient.getFirstName(),"Patient");
    }

    @Test
    void setFirstName() {
        patient.setFirstName("Fredrik");
        Assertions.assertEquals(patient.getFirstName(), "Fredrik");
    }

    @Test
    void getSurName() {
        Assertions.assertEquals(patient.getSurName(), "Yo");
    }

    @Test
    void setSurName() {
        patient.setSurName("Hei");
        Assertions.assertEquals(patient.getSurName(), "Hei");
    }

    @org.junit.jupiter.api.Test
    void getSocialSecurityNumber() {
        Assertions.assertEquals( "12345678900", patient.getSocialSecurityNumber());
    }

    @org.junit.jupiter.api.Test
    void setSocialSecurityNumber() {
        patient.setSocialSecurityNumber("12345678901");
        Assertions.assertEquals("12345678901", patient.getSocialSecurityNumber());
    }


    @org.junit.jupiter.api.Test
    void setDiagnosis() {
        patient.setDiagnosis("Mental retardation");
        Assertions.assertEquals("Mental retardation", patient.getDiagnosis());
    }

    @org.junit.jupiter.api.Test
    void getGeneralPractitioner() {
        Assertions.assertEquals("genprac", patient.getGeneralPractitioner());
    }

    @org.junit.jupiter.api.Test
    void setGeneralPractitioner() {
        patient.setGeneralPractitioner("genprac1");
        Assertions.assertEquals("genprac1", patient.getGeneralPractitioner());
    }

    @org.junit.jupiter.api.Test
    void testEquals() {
        Patient pat = new Patient("YO", "hei", "genprac", "12344567890");
        Patient pat1 = new Patient("YO", "hei", "genprac", "12344567890");
        Assertions.assertTrue(pat.equals(pat1));
    }
}